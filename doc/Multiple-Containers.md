# Running Multiple Containers

## 1. Testing Mysql In a Container

Before running muliptle containers, let's first run mysql in a container by itself.

When we run the mysql container, we will want it to have some tables.

So, we will get the mysql container install the Shippers tables into the database when it starts up.

1. cd home and create a directory to put the schema in
```
cd
mkdir schema
cd schema
```

2. download the schema sql file ```curl -L bit.ly/shippers-schema > schema.sql```

3. run the container
```
docker run -d --name mysql -v /home/grads/schema:/docker-entrypoint-initdb.d  -e MYSQL_ROOT_PASSWORD=c0nygre mysql
```

You should check it's running with docker ps, then get a cmd line INSIDE the container and run the mysql client to look around.

```
docker exec -it mysql /bin/bash
mysql -u root -pc0nygre
use Northwind;
show tables;
select * from Shippers;
exit
exit
```

We've proved we can start a database in a container, now stop and remove the container
```
docker stop mysql
docker rm mysql
```

## 2. Two Containers on a "docker network"

1. Create a docker network called "mysqlnet"
```
docker network create mysqlnet
```

2. Run the mysql container again, but put it on the network with the --network flag
```
docker run -d --name mysql --network mysqlnet -v /home/grads/schema:/docker-entrypoint-initdb.d  -e MYSQL_ROOT_PASSWORD=c0nygre mysql
```

3. Run your java app container, putting it on the network and telling it to use the Database Host called "mysql" (because that's the NAME of the container we created)
```
docker run --name javaapp --network mysqlnet -e DB_HOST=mysql -p 8081:8080 shippers-demo:0.0.1
```


## 3. Multiple Containers with docker-compose

docker-compose is a tool that lets us handle multipe containers as a single unit.

With docker-compose the containers, and how to run them, is all defined in yaml. The convention is for this file to be called docker-compose.yml

We could do all of the previous steps using docker-compose.

The docker-compose file for this example can be downloaded here:
```curl -L bit.ly/shippers-compose```

For reference the contents of the docker-compose.yml is:
```
version: '3.0'

services:

  mysql:
    image: mysql:latest
    volumes:
      - /home/grads/schema:/docker-entrypoint-initdb.d
    environment:
      MYSQL_ROOT_PASSWORD: c0nygre
    networks:
      - mysqlnet

  javaapp:
    image: shippers-demo:0.0.1
    environment:
      DB_HOST: mysql
    ports:
      - 8081:8080
    networks:
      - mysqlnet

networks:
  mysqlnet:
```
